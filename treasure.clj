(use 'clojure.java.io)

(def customMaze [])
(def curY 0)
(def curX 0)
(def treasureFound false)

(defn map_print [] 
	(doseq [line customMaze]
		(println line)
	)
)

(defn placePlus []
	(def line (get customMaze curX))
	(def line (str (subs line 0 curY) (str "+" (subs line (+ curY 1)(count line)))))
	(def customMaze (assoc customMaze curX line))
)

(defn checkDirectionToMove[]
	(def rowX_size (count customMaze))
	(def colY_size (count (get customMaze 0)))
	(cond
		(and (< (+ curY 1) colY_size) (=(get (get customMaze curX) (+ curY 1)) \-))
			(do
				(def curY (inc curY))
			)
		(and (< (+ curX 1) rowX_size) (= (get (get customMaze (+ curX 1)) curY) \-))
			(do
				(def curX (inc curX))
			)
		(and (>= (- curY 1) 0) (= (get (get customMaze curX) (- curY 1)) \-))
			(do
				(def curY (dec curY))
			)
		(and (>= (- curX 1) 0) (= (get (get customMaze (- curX 1)) curY) \-))
			(do
				(def curX (dec curX))
			)
	)
)

(defn checkTreasure []
	(def rowX_size (count customMaze))
	(def colY_size (count (get customMaze 0)))
	(cond
		(= (get (get customMaze curX) curY) \@)
			(do
				(def treasureFound true)
			)
		(and (< (+ curY 1) colY_size) (= (get (get customMaze curX) (+ curY 1)) \@))
			(do
				(def treasureFound true)
			)
		(and (< (+ curX 1) rowX_size) (= (get (get customMaze (+ curX 1)) curY) \@))
			(do
				(def treasureFound true)
			)
		(and (>= (- curY 1) 0) (= (get (get customMaze curX) (- curY 1)) \@))
			(do
				(def treasureFound true)
			)
		(and (>= (- curX 1) 0) (= (get (get customMaze (- curX 1)) curY) \@))
			(do
				(def treasureFound true)
			)
	)
)

(defn moveBackwards []
	(def rowX_size (count customMaze))
	(def colY_size (count (get customMaze 0)))
	(cond
		(and (< (+ curY 1) colY_size) (=(get (get customMaze curX) (+ curY 1)) \+))
			(do
				(def curY (inc curY))
			)
		(and (< (+ curX 1) rowX_size) (= (get (get customMaze (+ curX 1)) curY) \+))
			(do
				(def curX (inc curX))
			)
		(and (>= (- curY 1) 0) (= (get (get customMaze curX) (- curY 1)) \+))
			(do
				(def curY (dec curY))
			)
		(and (>= (- curX 1) 0) (= (get (get customMaze (- curX 1)) curY) \+))
			(do
				(def curX (dec curX))
			)
	)
)

(defn checkPathExists []
	(def existsPath false)
	(def rowX_size (count customMaze))
	(def colY_size (count (get customMaze 0)))
	(cond
		(and (< (+ curY 1) colY_size) (= (get (get customMaze curX) (+ curY 1)) \-))
			(do
				(def existsPath true)
			)
		(and (< (+ curX 1) rowX_size) (= (get (get customMaze (+ curX 1)) curY) \-))
			(do
				(def existsPath true)
			)
		(and (>= (- curY 1) 0) (= (get (get customMaze curX) (- curY 1)) \-))
			(do
				(def existsPath true)
			)
		(and (>= (- curX 1) 0) (= (get (get customMaze (- curX 1)) curY) \-))
			(do
				(def existsPath true)
			)
	)
	(if (not existsPath)
		(do
			(def line (get customMaze curX))
			(def line (str (subs line 0 curY) (str "!" (subs line (+ curY 1)(count line)))))
			(def customMaze (assoc customMaze curX line))
			(moveBackwards)
		)
		(do
			(checkDirectionToMove)
		)
	)
)

(defn solveMaze []
	(if(not treasureFound)
		(do
			(checkTreasure)
			(cond
				(= (get (get customMaze curX) curY) \-)
					(do
						(placePlus)
						(checkDirectionToMove)
						(solveMaze)
					)
				(= (get (get customMaze curX) curY) \#)
					(do
						(moveBackwards)
						(solveMaze)
					)
				(= (get (get customMaze curX) curY) \!)
					(do
						(moveBackwards)
					)
				:else
					(do
						(checkPathExists)
						(solveMaze)
					)
			)
		)
	)
)

(defn treasure []
	(if (.exists (file "map.txt"))
		(do
			(with-open [rdr (reader "map.txt")]
				(doseq [line (line-seq rdr)]
					(def customMaze (conj customMaze line))
				)
			)
		)
		(do
			(println "file not available")
		)
	)
	(def validation true)
	(def countCol (count(get customMaze 0)))
	
	(doseq [line customMaze]
		(if validation
			(do
				(if (not= countCol (count line))
					(do
						(def validation false)
					)
				)
				(doseq [character line]
					(if	(and (and (not= character \#) (not= character \-)) (not= character \@))
						(do
							(def validation false)
						)
					)
				)
			)
		)
	)
	
	(if validation
		(do
			(println "This is my challege")
			(map_print)
			(solveMaze)
			(if treasureFound
				(do
					(println "Treasure Found")
					(map_print)
				)
				(do
					(println "Treasure Not Found")
					(map_print)
				)
			)
		)
		(do
			(println "Invalid File")
		)
	)
)
(treasure)